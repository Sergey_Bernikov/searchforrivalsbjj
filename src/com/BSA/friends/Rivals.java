package com.BSA.friends;

import java.util.Objects;

public class Rivals {
    private String name;
    private Boolean flagrelax;
    // приводим к верхнему регистру
    public Rivals (String name){
        this.name = name.toUpperCase();
        this.flagrelax = false;
    }
    public void setName (String name){
        this.name = name;
    }
    public String getName (){
        return name;
    }

    public Boolean getFlagrelax() {
        return flagrelax;
    }

    public void setFlagrelax(Boolean flagrelax) {
        this.flagrelax = flagrelax;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rivals rivals = (Rivals) o;
        return Objects.equals(name, rivals.name) && Objects.equals(flagrelax, rivals.flagrelax);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, flagrelax);
    }

    @Override
    public String toString() {
        return name;
    }
}
