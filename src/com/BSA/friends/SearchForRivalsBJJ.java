package com.BSA.friends;

import java.util.*;

public class SearchForRivalsBJJ {
    public static void main(String[] args) {
        SearchForRivalsBJJ searchForRivalsBJJ = new SearchForRivalsBJJ();
        searchForRivalsBJJ.run();
    }
    public void run(){
        int numberOfRivals;                                                                         // переменная - количество соперников
        while (true) {
            numberOfRivals = WorkingWithMethods.numberOfRivals();                                   // метод - ввод количество соперников, проверка на корректность ввода
           if (numberOfRivals == -1) continue;
           break;
        }

        int numberOfRounds;                                                                         // переменная - количество поединков
        while (true) {
            numberOfRounds = WorkingWithMethods.numberOfRounds(numberOfRivals);                     // метод - ввод количество поединков, проверка на кооректность ввода
            if (numberOfRounds == -1) continue;
            break;
        }

       List <Rivals> listOfRivals = WorkingWithMethods.areEnteringDataAboutTheRivals(numberOfRivals);             // переменная со списком соперников / метод - ввод данных о соперниках в коллекцию List

        List mainList = new ArrayList(100);                                                           // общий список пар (FIGHT)

        for (int i = 0; i < numberOfRounds; i++){
            if (WorkingWithMethods.random(listOfRivals, numberOfRivals, mainList).size() == 0) i--;              // метод - рандомный выбор соперников (повторы соперников исключены)
        }

        int counterForAllRound = 0;
        int counterForOneRound = 1;
        for (int i = 0; i < numberOfRounds; i++){

            System.out.println("*******************************************\nFIGHT - " + (i+1) + "\n*******************************************");

            while (counterForAllRound < ((numberOfRivals/2)*2) * counterForOneRound){
                if (counterForAllRound % 2 == 0) System.out.println(mainList.get(counterForAllRound));
                counterForAllRound++;
                }
            counterForOneRound++;
        }
    }
}