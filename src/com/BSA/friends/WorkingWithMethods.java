package com.BSA.friends;

import java.util.*;

public class WorkingWithMethods {

    // метод - ввод количество соперников, проверка на корректность ввода
    public static int numberOfRivals(){                     // *** можно в файл и на печать!!!
        Scanner console = new Scanner(System.in);
        System.out.print("Введите количество противников (не менее 3): ");
        int quantity = 0;
        try {
            quantity = console.nextInt();
            if (quantity > 300||quantity < 3) throw new ALargeNumberException(1);
        }
        catch (InputMismatchException e){
            System.out.println("Ошибка ввода. Введите целое число больше двух");
            return -1;
        }
        catch (ALargeNumberException e) {
            return -1;
        }
        return quantity;
    }

    // метод - ввод количество поединков, проверка на корректность ввода
    public static int numberOfRounds(int numberOfRivals){
        Scanner console = new Scanner(System.in);
        System.out.print("Введите количество раундов не превышающее или равное количество соперников: ");
        int quantity = 0;
        try {
            quantity = console.nextInt();
            if (quantity >= numberOfRivals) throw new ALargeNumberException();
            if (quantity <= 0) throw new ALargeNumberException("error");
        }
        catch (InputMismatchException e){
            System.out.println("Ошибка ввода. Введите целое число больше нуля");
            return -1;
        }
        catch (ALargeNumberException e) {
            return -1;
        }
        return quantity;
    }


    // Ввод данных о соперниках в коллекцию List
    public static List areEnteringDataAboutTheRivals(int numberOfRivals){
        Scanner console = new Scanner(System.in);
        List <Rivals> listOfRivals = new ArrayList<>();
        System.out.println("!!! Введите индивидуальные данные соперников !!!");
        for (int i = 0; i < numberOfRivals; i++){
            System.out.print(i + 1 + ". ");
            Rivals rivals = new Rivals(console.nextLine());
            if (!listOfRivals.contains(rivals)) {                                     // проверка соперника в коллекции (если нет - добавить)
                listOfRivals.add(rivals);
            }
            else {
                System.out.println("Такой оппонент уже зарегистрирован, повторите ввод");
                i--;
            }
        }
        return listOfRivals;
    }

    // метод - рандомный выбор соперников (повторы соперников исключены)
    public static List random(List listOfRivals, int numberOfRivals, List mainList){
        Random random = new Random();

        int x = 301;
        int y = 301;
        int count = 0;
        int countHumans = 0;
        int numberHuman = numberOfRivals;
        int countExit = 0;                                  // для сброса раунда - перераспределение

        List <Integer> listX = new ArrayList<>();
        List <Integer> listY = new ArrayList<>();

        while (x == y) {

            while (!listX.contains(x-1)) {
                x = random.nextInt(listOfRivals.size());
                if (listX.contains(x)) {
                    x = 301;
                    continue;
                }
                listX.add(x);
                listY.add(x);
                break;
            }


            while (!listX.contains(-1)) {
                y = random.nextInt(listOfRivals.size());
                if (listY.contains(y)){
                        y = 301;
                        continue;
                }
                listX.add(y);
                listY.add(y);
                break;
            }

            /* ************************** Проверка коллекции на совпадения и сброса раунда для перераспределения ***************************** */
            if (mainList.contains(listOfRivals.get(x) + " <-- VS -->" + " " + listOfRivals.get(y))||mainList.contains(listOfRivals.get(y) + " <-- VS -->" + " " + listOfRivals.get(x))){
                count++;

                countExit++;
                if  (countExit > 1000){
                    mainList.clear();
                    return mainList;
                }

                if (count > 10){
                    int sizeArray = listX.size();

                        listX.clear();
                        listY.clear();

                    for (int i = 0; i < countHumans; i++) {
                        mainList.remove(mainList.size() - 1);
                    }
                    countHumans = 0;
                    count = 0;
                    numberOfRivals = numberHuman;
                    x = 301;
                    y = 301;
                    continue;
                }
                listX.remove(listX.size() - 1);
                listY.remove(listY.size() - 1);
                listX.remove(listX.size() - 1);
                listY.remove(listY.size() - 1);
                x = 301;
                y = 301;
                continue;
            }
            /* ***************************************************************************************************************************** */

            mainList.add(listOfRivals.get(x) + " <-- VS -->" + " " + listOfRivals.get(y));
            countHumans++;
            numberOfRivals--;
            mainList.add(listOfRivals.get(y) + " <-- VS -->" + " " + listOfRivals.get(x));
            countHumans++;
            numberOfRivals--;

             if (numberOfRivals == 1||numberOfRivals == 0){
                x = 301;
                y = 301;
                break;
            }

            if (numberOfRivals != 1||numberOfRivals != 0){
                x = 301;
                y = 301;
            }

        }
        return mainList;
    }

}
